#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

void checkShader(unsigned int index, int success, char infoLog[512]);

/* SETTINGS: Screen dimensions */
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

/* GLSL: Shaders */
const char* vertexShaderSource = "#version 460 core\n"
	"layout (location = 0) in vec3 aPos;\n"
	"void main() { gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0); }\0";

const char* fragmentShaderSource = "#version 460 core\n"
	"out vec4 FragColor;\n"
	"void main() { FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f); }\0";

int main()
{
	/* GLFW: Initialize and configure */
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	/* GLFW: Create window */
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	/* GLAD: Load all OpenGL function pointers */
	if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	/* OPENGL: Shader check */
	int success;
	char infoLog[512];

	/* OPENGL: Vertex Shader */
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	checkShader(vertexShader, success, infoLog);

	/* OPENGL: Fragment Shader */
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	checkShader(fragmentShader, success, infoLog);

	/* OPENGL: Shader Program */
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	checkShader(shaderProgram, success, infoLog);

	/* OPENGL: Set up vertex data */
	float vertices[] = {
		 0.5f,  0.5f, 0.0f, /* Top right */
		 0.5f, -0.5f, 0.0f, /* Bottom right */
		-0.5f, -0.5f, 0.0f, /* Bottom left */
		-0.5f,  0.5f, 0.0f  /* Top left */
	};
	unsigned int indices[] = {
		0, 1, 3, /* First triangle */
		1, 2, 3  /* Second triangle */
	};

	/* OPENGL: Set up element buffer(s) */
	unsigned int EBO;
	glGenBuffers(1, &EBO);

	/* OPENGL: Set up vertex array(s) */
	unsigned int VAO;
	glGenVertexArrays(1, &VAO);

	/* OPENGL: Set up vertex buffer(s) */
	unsigned int VBO;
	glGenBuffers(1, &VBO);

	/* OPENGL: Bind Vertex Array Object */
	glBindVertexArray(VAO);

	/* OPENGL: Copy vertices array to a vertex buffer */
	glBindBuffer(GL_ARRAY_BUFFER, VBO); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	/* OPENGL: Copy indices array to an element buffer */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	/* OPENGL: Set vertex attributes pointers */
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*) 0);
	glEnableVertexAttribArray(0);

	/* OPENGL: Wireframe mode */
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	/* GLFW: Loop render */
	while (!glfwWindowShouldClose(window))
	{
		/* GLFW: Input */
		processInput(window);
		
		/* OPENGL: Background color */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		/* OPENGL: Draw triangle */
		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		/* GLFW: Swap buffers and poll IO events */
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	/* GLFW: Clear previously allocated GLFW resources */
	glfwTerminate();

	return 0;
}

/* GLFW: React to input */
void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
}

/* GLFW: Execute this function when the window size changes */
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	/* OPENGL: Match viewport dimensions to window dimensions */
	glViewport(0, 0, width, height);
}

/* OPENGL: Check shader success */
void checkShader(unsigned int index, int success, char infoLog[512])
{
	if (!success)
	{
		glGetShaderInfoLog(index, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog;
	}
}
